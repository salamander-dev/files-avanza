Requerimientos:

 

Crear un webservice API REST que pueda gestionar ficheros de acuerdo a lo siguiente:

 

Ver lista de ficheros

Crear un nuevo fichero

Subir un fichero

Limitar el fichero 500kb

Eliminar fichero de forma lógica

Eliminar fichero de forma física

Subir ficheros de forma masiva

 

Consideraciones:

El uso de el endpoint debe estar protegido por token

Se debe contabilizar el uso

Se debe limitar a 3 consultas por minuto

 

Se tomará en cuenta:

Uso de buenas prácticas y clean code

Testing

POO

 

Un plus:

Patrones de diseño

TDD

DDD