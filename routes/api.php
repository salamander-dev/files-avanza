<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\V1\FileController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    //Prefijo V1 /api/v1/ no require auth*
    Route::post('login', [AuthController::class, 'authenticate']);
    Route::post('register', [AuthController::class, 'register']);
    Route::group(['middleware' => ['jwt.verify']], function() {
        // /api/v1/ require auth*
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('get-user', [AuthController::class, 'getUser']);
        Route::apiResource('files', FileController::class);
        Route::delete('destroy-file', [FileController::class, 'destroyFile']);
        Route::post('upload-files', [FileController::class, 'uploadFiles']);
        /*
        Route::post('products', [FileController::class, 'store']);
        Route::put('products/{id}', [FileController::class, 'update']);
        Route::delete('products/{id}', [FileController::class, 'destroy']);*/
    });
});
