<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\FilePostRequest;
use Illuminate\Http\Request;
use App\Models\File;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;


class FileController extends Controller
{
    protected $file;

    public function __construct(Request $request)
    {
        $token = $request->header('Authorization');
        if($token != '')
            $this->file = JWTAuth::parseToken()->authenticate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return File[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index()
    {
        return File::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FilePostRequest $request)
    {

        $file = new File;
        $url = $request->file('url')->store('files-api');
        $size = $request->file("url")->getSize();
        $size = number_format($size / 1024,2);
        $file->create($request->all());

        $last_id = File::latest()->first()->id;
        $saveFile = File::find($last_id);
        $saveFile->url = $url;
        $saveFile->weight = $size;
        $saveFile->save();

        return response()->json(
            [
                'message' => 'Success register file.'
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        return $file::whereId($file->id)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FilePostRequest $request, File $file)
    {
        $url = $request->file('url')->store('files-api');
        $size = $request->file("url")->getSize();
        $size = number_format($size / 1024,2);
        $file->update($request->all());
        $saveFile = File::find($request->id);
        $saveFile->url = $url;
        $saveFile->weight = $size;
        $saveFile->save();
        return response()->json(
            [
                'message' => 'File updated successfully :).'
            ]
        );
    }

    public function uploadFiles(FilePostRequest $request){
        $uploads = $request->file('url');
        foreach($uploads as $upload) {
            $file = new File;
            $url = $request->file('url')->store('files-api');
            $size = $request->file("url")->getSize();
            $size = number_format($size / 1024, 2);
            $file->create($request->all());

            $last_id = File::latest()->first()->id;
            $saveFile = File::find($last_id);
            $saveFile->url = $url;
            $saveFile->weight = $size;
            $saveFile->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        $file->delete();
    }

    public function destroyFile(File $file)
    {
        Storage::delete($file->url);
    }
}
